const button = document.getElementById('button');
const input1 = document.getElementById('num1')! as HTMLInputElement;
const input2 = document.getElementById('num2')! as HTMLInputElement;

button.addEventListener('click', function () {
    console.log(add(+input1.value, +input2.value)); // 1 + 2 = 12 (js)
})


function add(num1: number, num2: number) {
    return num1 + num2;
}

console.log(add(2, 3)); // 23