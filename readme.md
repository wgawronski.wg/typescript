ts :  jaki problem rozwiązuje? 
by Microsoft

https://www.youtube.com/watch?v=c-HvzaZ3lb4  
https://www.youtube.com/watch?v=BwuLxPH8IDs&t=3843s
https://www.youtube.com/watch?v=puOKhYcPg-0

Wychwytuje błędy na etapie kompilacji.

Opakowanie js typami podczas kompilacji: sprawdzenie czy moja intencja vs implementacja którą dostarczam jest zgodna. Dostajemy JS minus typy =  dekorowanie kodu typami a nie komentarzami.

Wychwytywanie błędów w czasie kompilacji a nie w czasie run-time: nie trzeba przeklikiwać aplikacji.

Testy: dobrze napisoane i dużo 
Jednostkowe: np. czy zwracany typ (czy dany konkrakt) jakiejś funkcji jest spełniony, czy właściwej struktury, atrybuty..
Jeżeli język jest statycznie typowany to testy tego typu nie są koniecznie potrzebne. Dużą cześć UnitTestów przejmuje compilator.

Relesowanie kodu to dopiero początek. Utrzymanie. Informacja o typie jest b istotną informacją dla ludzi którzy będą go czytali: więcej informacji co poeta miał na myśli.

TS do JS do silnika v8 (node / przeglądarka)


- serverSide, no browser
- webpack with ts
- react with ts
- best syntax (types)
- es5 vs es6 vs typeScript  
- ts -> js: ts compiler: `tsc ts.ts;`
- ts with react / node / express 
- less error prone, run time errors
- cleaner code
- Classes, Interfaces, 
- Generics,
- Decorators (adnotacje)
- js no die (func, arrays, promisy, assync await) -> ts: only control of fypes

Na początek zamiana .js na .ts a później dopisywanie typów.

```
npm install typescript --save-dev;
apt install node-typescript;
sudo npm install -g typescript;  
tsc ts.ts;
```

*make your code more readable*  
*and your life as developer easier*  

